﻿using System;

namespace Noroff_Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        //Prompting the user for input, and calling method for printing square.
        public static void Run() {

            int inputSize;

            Console.Write("Enter the size of the square: ");
            inputSize = Convert.ToInt32(Console.ReadLine()); //no validation tho D: 

            PrintSquare(inputSize);
        }

        //Prints a square of #'s with size equal to input.
        public static void PrintSquare(int inputSize)
        {
            for (int i = 0; i < inputSize; i++)
            {
                for (int j = 0; j < inputSize; j++)
                {
                    if (i == 0 || j == 0 || i == inputSize - 1 || j == inputSize - 1) {
                        Console.Write("#");
                    } else
                    if (i == 0 || j == 0 || i == inputSize - 1 || j == inputSize - 1)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
