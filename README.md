# Noroff Task 3 AND 8

Task 3: Draw a Square
Write a program which can be used to print a square of size chosen by the user at
run-time.
The square can be made of any character that you choose. (# is probably a good
choice)
The file must compile without errors. example: (after compilation)
I use _ to show a space here (Yours should be blank)
#####
#___#
#___#
#___#
#####

Task 8: Upgrades
� Modify your draw square solution to utilize methods